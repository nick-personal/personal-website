<?php

namespace app\models;

/**
 * This is the model class for table "skills".
 *
 * @property int $id
 * @property string $name
 * @property int $type
 * @property string $icon
 * @property int $level
 * @property string $description
 * @property string $updated_at
 * @property string $created_at
 */
class Skill extends \yii\db\ActiveRecord
{

	public static function tableName()
	{
		return 'skills';
	}

	public static function typesMap()
	{
		return [
			0 => 'No type',
			1 => 'Technical',
			2 => 'Pesonal',
		];
	}

	public function rules()
	{
		return [
			[['type', 'level'], 'integer'],
			[['updated_at', 'created_at'], 'safe'],
			[['name', 'icon', 'description'], 'string', 'max' => 255],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Name',
			'type' => 'Type',
			'icon' => 'Icon',
			'level' => 'Level',
			'description' => 'Description',
			'updated_at' => 'Updated At',
			'created_at' => 'Created At',
		];
	}

	public function getType()
	{
		$types = self::typesMap();

		return $types[$this->type];
	}

}
