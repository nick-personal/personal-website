<?php

namespace app\models;

use yii\helpers\Html;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $photo_url
 * @property string $resume_url
 * @property string $summary
 * @property string $phone
 * @property string $email
 * @property string $location
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $linkedin
 * @property string $github
 * @property string $gitlab
 * @property string $updated_at
 * @property string $created_at
 */
class User extends \yii\db\ActiveRecord
{

	public static function tableName()
	{
		return 'users';
	}

	public function rules()
	{
		return [
			[['summary'], 'string'],
			[['updated_at', 'created_at'], 'safe'],
			[['first_name', 'last_name', 'photo_url', 'resume_url', 'phone', 'email', 'location', 'facebook', 'twitter', 'instagram', 'linkedin', 'github', 'gitlab'], 'string', 'max' => 255],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'photo_url' => 'Photo Url',
			'resume_url' => 'Resume Url',
			'summary' => 'Summary',
			'phone' => 'Phone',
			'email' => 'Email',
			'location' => 'Location',
			'facebook' => 'Facebook',
			'twitter' => 'Twitter',
			'instagram' => 'Instagram',
			'linkedin' => 'Linkedin',
			'github' => 'Github',
			'gitlab' => 'Gitlab',
			'updated_at' => 'Updated At',
			'created_at' => 'Created At',
		];
	}

	public function getEducation()
	{
		return $this->hasMany(Education::class, ['user_id' => 'id'])->orderBy('date_start DESC');
	}

	public function getJobs()
	{
		return $this->hasMany(Job::class, ['user_id' => 'id'])->orderBy('date_start DESC');
	}

	public function getProjects()
	{
		return $this->hasMany(Project::class, ['user_id' => 'id']);
	}

	public function getSkills()
	{
		return Skill::find()->all();
	}

	public function getFullName()
	{
		return "{$this->first_name} {$this->last_name}";
	}

	public function getPhotoUrl()
	{
		return '/images/photo.jpg';
	}

	public function hasResume()
	{
		return true;
		return (!empty($this->resume_url) and file_exists($this->getResumeUrl()));
	}

	public function getResumeUrl()
	{
		return '/files/' . $this->resume_url;
	}

	public function renderPhoneLink()
	{
		return Html::a($this->phone, 'tel:' . $this->phone);
	}

	public function renderEmailLink()
	{
		return Html::a($this->email, 'mailto:' . $this->email);
	}

	public function renderSocialLinks()
	{
		$output = '';
		$links = ['facebook', 'twitter', 'instagram', 'linkedin', 'github', 'gitlab'];

		foreach ($links as $link)
		{
			$i = Html::tag('i', '', ['class' => 'fa fa-' . $link]);
			$output .= Html::a($i, $this->$link, ['target' => '_blank']);
		}

		return $output;
	}

}
