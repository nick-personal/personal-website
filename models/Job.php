<?php

namespace app\models;

use app\components\HistoryTrait;

/**
 * This is the model class for table "jobs".
 *
 * @property int $id
 * @property int $user
 * @property string $date_start
 * @property string $date_end
 * @property string $company_name
 * @property string $position
 * @property string $company_description
 * @property string $description
 * @property string $skills
 * @property string $updated_at
 * @property string $created_at
 */
class Job extends \yii\db\ActiveRecord
{
	use HistoryTrait;

    public static function tableName()
    {
        return 'jobs';
    }

    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['date_start', 'date_end', 'updated_at', 'created_at'], 'safe'],
            [['description'], 'string'],
            [['company_name', 'company_description', 'position', 'skills'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'company_name' => 'Company Name',
			'company_description' => 'Company Description',
            'position' => 'Position',
            'description' => 'Description',
            'skills' => 'Skills',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

//	public function getSkills()
//	{
//		$skills = explode(',', $this->skills);
//
//		return Skill::find()->where(['id' => $skills]);
//    }

}
