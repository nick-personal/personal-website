<?php

namespace app\models;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $image
 * @property string $description
 * @property string $link
 * @property string $source_link
 * @property string $skills
 * @property string $updated_at
 * @property string $created_at
 */
class Project extends \yii\db\ActiveRecord
{

	public static function tableName()
	{
		return 'projects';
	}

	public function rules()
	{
		return [
			[['user_id'], 'integer'],
			[['updated_at', 'created_at'], 'safe'],
			[['name', 'image', 'description', 'link', 'source_link', 'skills'], 'string', 'max' => 255],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'user_id' => 'User ID',
			'name' => 'Name',
			'image' => 'Image',
			'description' => 'Description',
			'link' => 'Link',
			'source_link' => 'Source Link',
			'skills' => 'Skills',
			'updated_at' => 'Updated At',
			'created_at' => 'Created At',
		];
	}

	public function getImageUrl()
	{
		return '/images/portfolio/1.jpg';
	}

	public function renderModal()
	{
		return \Yii::$app->controller->renderPartial('/partials/project_modal', ['project' => $this]);
	}

}
