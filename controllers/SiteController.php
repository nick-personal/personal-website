<?php

namespace app\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use app\models\{
	Project,
	User,
	LoginForm,
};

class SiteController extends \yii\web\Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::class,
				'only' => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => \yii\filters\VerbFilter::class,
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => \yii\web\ErrorAction::class,
				//'layout'
			],
			'captcha' => [
				'class' => \yii\captcha\CaptchaAction::class,
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex()
	{
		$user = User::find()
			->with([
				'education',
				'jobs',
				'projects',
			])
			->where(['id' => 1])
			->one();
		
		return $this->render('index', compact('user'));
	}

	public function actionProjects($id)
	{
		$project = Project::findOne($id);

		if (empty($project))
			throw new NotFoundHttpException();

		return $this->renderAjax('project', compact('project'));
	}

	public function actionLogin()
	{
		if (!Yii::$app->user->isGuest)
		{
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login())
		{
			return $this->goBack();
		}

		$model->password = '';
		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}

}
