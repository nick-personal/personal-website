<?php

namespace app\controllers;

/**
 * Description of AdminController
 *
 * @author lennakz
 */
class AdminController extends \yii\web\Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
}
