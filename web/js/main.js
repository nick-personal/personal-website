(function($) {
	'use strict';

	// Portfolio subpage filters
	function portfolio_init() {
		var portfolio_grid = $('#portfolio_grid'),
				portfolio_filter = $('#portfolio_filters');

		if (portfolio_grid) {

			portfolio_grid.shuffle({
				speed: 450,
				itemSelector: 'figure'
			});

			$('.site-main-menu').on('click', 'a', function() {
				portfolio_grid.shuffle('update');
			});


			portfolio_filter.on('click', '.filter', function(e) {
				portfolio_grid.shuffle('update');
				e.preventDefault();
				$('#portfolio_filters .filter').parent().removeClass('active');
				$(this).parent().addClass('active');
				portfolio_grid.shuffle('shuffle', $(this).attr('data-group'));
			});

		}
	}

	// Hide Mobile menu
	function mobileMenuHide() {
		var windowWidth = $(window).width();
		if (windowWidth < 1024) {
			$('#site-header').addClass('mobile-menu-hide');
		}
	}

	//On Window load & Resize
	$(window)
		.on('resize', function() {
			mobileMenuHide();
		})
		.scroll(function() {
			if ($(window).scrollTop() < 20) {
				$('.header').removeClass('sticked');
			} else {
				$('.header').addClass('sticked');
			}
		})
		.scrollTop(0);

	// On Document Load
	$(document).on('ready', function () {
		// Initialize Portfolio grid
		var $portfolio_container = $('#portfolio-grid');

		$portfolio_container.imagesLoaded(function() {
			setTimeout(function() {
				portfolio_init(this);
			}, 500);
		});

		// Portfolio hover effect init
		$('#portfolio_grid > figure').each(function() {
			$(this).hoverdir();
		});

		// Blog grid init
		setTimeout(function() {
			var $container = $('.blog-masonry');
			$container.masonry();
		}, 500);

		// Mobile menu
		$('.menu-toggle').click(function() {
			$('#site-header').toggleClass('mobile-menu-hide');
		});

		// Mobile menu hide on main menu item click
		$('.site-main-menu').on('click', 'a', function() {
			mobileMenuHide();
		});

		// Sidebar toggle
		$('.sidebar-toggle').click(function() {
			$('#blog-sidebar').toggleClass('open');
		});

		// Text rotation
		$('.text-rotation').owlCarousel({
			loop: true,
			dots: false,
			nav: false,
			margin: 0,
			items: 1,
			autoplay: true,
			autoplayHoverPause: false,
			autoplayTimeout: 3800,
			animateOut: 'zoomOut',
			animateIn: 'zoomIn'
		});

		// Lightbox init
		$('body').magnificPopup({
			delegate: 'a.lightbox',
			type: 'image',
			removalDelay: 300,

			// Class that is added to popup wrapper and background
			// make it unique to apply your CSS animations just to this exact popup
			mainClass: 'mfp-fade',
			image: {
				// options for image content type
				titleSrc: 'title',
				gallery: {
					enabled: true
				},
			},

			iframe: {
				markup: '<div class="mfp-iframe-scaler">' +
						'<div class="mfp-close"></div>' +
						'<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
						'<div class="mfp-title mfp-bottom-iframe-title"></div>' +
						'</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button

				patterns: {
					youtube: {
						index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

						id: null, // String that splits URL in a two parts, second part should be %id%
						// Or null - full URL will be returned
						// Or a function that should return %id%, for example:
						// id: function(url) { return 'parsed id'; }

						src: '%id%?autoplay=1' // URL that will be set as a source for iframe.
					},
					vimeo: {
						index: 'vimeo.com/',
						id: '/',
						src: '//player.vimeo.com/video/%id%?autoplay=1'
					},
					gmaps: {
						index: '//maps.google.',
						src: '%id%&output=embed'
					}
				},

				srcAction: 'iframe_src', // Templating object key. First part defines CSS selector, second attribute. "iframe_src" means: find "iframe" and set attribute "src".
			},

			callbacks: {
				markupParse: function (template, values, item) {
					values.title = item.el.attr('title');
				}
			},
		});

		$('.ajax-page-load-link').magnificPopup({
			type: 'inline',
			removalDelay: 300,
			mainClass: 'mfp-fade',
			gallery: {
				enabled: true
			},
		});

		//Form Controls
		$('.form-control')
				.val('')
				.on('focusin', function () {
					$(this).parent('.form-group').addClass('form-group-focus');
				})
				.on('focusout', function () {
					if ($(this).val().length === 0) {
						$(this).parent('.form-group').removeClass('form-group-focus');
					}
				});

		// Smooth scrolling from navbar
		$('#navbar a').click(function(e) {
			e.preventDefault();
			var hash = $(this).prop('hash');
			$('html, body').animate({
				scrollTop: $(hash).offset().top - 100,
			}, 500);
		});

		$('body').scrollspy({
			target: '#navbar',
			offset: 100,
		});

		$('.portfolio-page-carousel').owlCarousel({
			smartSpeed: 1200,
			items: 1,
			loop: true,
			dots: true,
			nav: true,
			navText: false,
			margin: 10
		});

	});

})(jQuery);
