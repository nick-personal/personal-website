<?php

function smarty_modifier_attributes($value)
{
	return \yii\helpers\VarDumper::dumpAsString($value->attributes, 10, true);
}
