<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_skills`.
 */
class m181202_045011_create_users_skills_table extends Migration
{

	public function safeUp()
	{
		$this->createTable('users_skills', [
			'id' => $this->primaryKey(),
			'user_id' => $this->integer(),
			'skill_id' => $this->integer(),
			'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->after('ON UPDATE CURRENT_TIMESTAMP'),
			'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
		]);
	}

	public function safeDown()
	{
		$this->dropTable('users_skills');
	}

}
