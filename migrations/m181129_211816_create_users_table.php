<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m181129_211816_create_users_table extends Migration
{

	public function safeUp()
	{
		$this->createTable('users', [
			'id' => $this->primaryKey(),
			'first_name' => $this->string(),
			'last_name' => $this->string(),
			'photo_url' => $this->string(),
			'resume_url' => $this->string(),
			'summary' => $this->text(),
			'phone' => $this->string(),
			'email' => $this->string(),
			'location' => $this->string(),
			'facebook' => $this->string(),
			'twitter' => $this->string(),
			'instagram' => $this->string(),
			'linkedin' => $this->string(),
			'github' => $this->string(),
			'gitlab' => $this->string(),
			'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->after('ON UPDATE CURRENT_TIMESTAMP'),
			'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
		]);
	}

	public function safeDown()
	{
		$this->dropTable('users');
	}

}
