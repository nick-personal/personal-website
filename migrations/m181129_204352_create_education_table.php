<?php

use yii\db\Migration;

/**
 * Handles the creation of table `education`.
 */
class m181129_204352_create_education_table extends Migration
{

	public function safeUp()
	{
		$this->createTable('education', [
			'id' => $this->primaryKey(),
			'user_id' => $this->integer(),
			'date_start' => $this->date(),
			'date_end' => $this->date(),
			'institute_name' => $this->string(),
			'institute_logo' => $this->string(),
			'institute_link' => $this->string(),
			'location' => $this->string(),
			'skills' => $this->string(),
			'field' => $this->string(),
			'certificate' => $this->string(),
			'description' => $this->text(),
			'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->after('ON UPDATE CURRENT_TIMESTAMP'),
			'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
		]);
	}

	public function safeDown()
	{
		$this->dropTable('education');
	}

}
