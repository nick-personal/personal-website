<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projects`.
 */
class m181129_204322_create_projects_table extends Migration
{

	public function safeUp()
	{
		$this->createTable('projects', [
			'id' => $this->primaryKey(),
			'user_id' => $this->integer(),
			'name' => $this->string(),
			'image' => $this->string(),
			'description' => $this->string(),
			'link' => $this->string(),
			'source_link' => $this->string(),
			'skills' => $this->string(),
			'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->after('ON UPDATE CURRENT_TIMESTAMP'),
			'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
		]);
	}

	public function safeDown()
	{
		$this->dropTable('projects');
	}

}
