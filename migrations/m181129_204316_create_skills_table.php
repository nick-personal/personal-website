<?php

use yii\db\Migration;

/**
 * Handles the creation of table `skills`.
 */
class m181129_204316_create_skills_table extends Migration
{

	public function safeUp()
	{
		$this->createTable('skills', [
			'id' => $this->primaryKey(),
			'name' => $this->string(),
			'type' => $this->integer()->defaultValue(0),
			'icon' => $this->string(),
			'level' => $this->integer(),
			'description' => $this->string(),
			'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->after('ON UPDATE CURRENT_TIMESTAMP'),
			'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
		]);
	}

	public function safeDown()
	{
		$this->dropTable('skills');
	}

}
