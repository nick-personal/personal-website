<?php

use yii\db\Migration;

/**
 * Handles the creation of table `jobs`.
 */
class m181129_204338_create_jobs_table extends Migration
{

	public function safeUp()
	{
		$this->createTable('jobs', [
			'id' => $this->primaryKey(),
			'user_id' => $this->integer(),
			'date_start' => $this->date(),
			'date_end' => $this->date(),
			'company_name' => $this->string(),
			'company_description' => $this->string(),
			'position' => $this->string(),
			'description' => $this->text(),
			'skills' => $this->string(),
			'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->after('ON UPDATE CURRENT_TIMESTAMP'),
			'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
		]);
	}

	public function safeDown()
	{
		$this->dropTable('jobs');
	}

}
