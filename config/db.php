<?php

return [
	'class' => \yii\db\Connection::class,
	'dsn' => 'sqlite:' . realpath(__DIR__ . '/../data/data.db'),
	'charset' => 'utf8',
	// Schema cache options (for production environment)
	//'enableSchemaCache' => true,
	//'schemaCacheDuration' => 60,
	//'schemaCache' => 'cache',
];
