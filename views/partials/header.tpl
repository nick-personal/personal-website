<header id="site-header" class="header mobile-menu-hide">
	<div class="header-content">
		<div class="site-title-block mobile-hidden">
			<div class="site-title">Nick <span>Kotok</span></div>
		</div>
		<div id="navbar" class="site-nav">
			<ul id="nav" class="nav site-main-menu">
				<li><a href="#about">Home</a></li>
				<li><a href="#portfolio">Portfolio</a></li>
				<li><a href="#experience">Experience</a></li>
				<li><a href="#education">Education</a></li>
				<li><a href="#skills">Skills</a></li>
				<li><a href="#contact">Contact</a></li>
			</ul>
		</div>
	</div>
</header>

<div class="mobile-header mobile-visible">
	<div class="mobile-logo-container">
		<div class="mobile-site-title">Nick Kotok</div>
	</div>
	<a class="menu-toggle mobile-visible">
		<i class="fa fa-bars"></i>
	</a>
</div>