{$this->beginPage()}
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Nick Kotok's personal website, includes work experience, projects, education and constacts">
		<meta name="keywords" content="">
		{\yii\helpers\Html::csrfMetaTags()}
		<title>Nick Kotok Personal Website</title>

		<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="css/normalize.css" type="text/css">
		<link rel="stylesheet" href="css/animate.css" type="text/css">
		<link rel="stylesheet" href="css/transition-animations.css" type="text/css">
		<link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
		<link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
		<link rel="stylesheet" href="css/main.css" type="text/css">

		<script src="js/jquery-2.1.3.min.js"></script>
		<script src="js/modernizr.custom.js"></script>

		{$this->head()}

		<link rel="shortcut icon" href="favicon.ico">
	</head>
	<body class="material-template">
		{$this->beginBody()}

		<div id="page" class="page">

			{partial view='/partials/header'}

			{$content}

		</div>

		{partial view='/partials/footer'}

		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
		<script type="text/javascript" src="js/validator.js"></script>
		<script type="text/javascript" src="js/jquery.shuffle.min.js"></script>
		<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
		<script type="text/javascript" src="js/owl.carousel.min.js"></script>
		<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
		<script type="text/javascript" src="js/jquery.hoverdir.js"></script>
		<script type="text/javascript" src="js/main.js"></script>

		{$this->endBody()}
	</body>
</html>
{$this->endPage()}