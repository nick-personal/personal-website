<div id="main" class="site-main">
	<div class="pt-wrapper">
		<div class="subpages">

			<section class="pt-page" id="about">
				<div class="section-inner start-page-content">
					<div class="page-header">
						<div class="row">
							<div class="col-sm-4 col-md-4 col-lg-4">
								<div class="photo">
									<img src="{$user->getPhotoUrl()}" alt="{$user->getFullName()} photo.">
								</div>
							</div>
							<div class="col-sm-8 col-md-8 col-lg-8">
								<div class="title-block">
									<h1>{$user->getFullName()}</h1>
									<div class="owl-carousel text-rotation">
										{foreach $user->getSkills() as $m}
											<div class="item">
												<div class="sp-subtitle">{$m->name|escape}</div>
											</div>
										{/foreach}
									</div>
								</div>
								<div class="social-links">
									{$user->renderSocialLinks()}
								</div>
							</div>
						</div>
					</div>
					<div class="page-content">
						<div class="row">
							<div class="col-sm-6 col-md-6 col-lg-6">
								<div class="about-me">
									<div class="block-title">
										<h3>About <span>Me</span></h3>
									</div>
									<p>{$user->summary|escape}</p>
								</div>
								{if $user->hasResume()}
									<div class="download-resume">
										<a href="{$user->getResumeUrl()}" class="btn btn-secondary">Download Resume</a>
									</div>
								{/if}
							</div>
							<div class="col-sm-6 col-md-6 col-lg-6">
								<ul class="info-list">
									<li><span class="title">Age</span><span class="value">29</span></li>
									<li><span class="title">Location</span><span class="value">{$user->location|escape}</span></li>
									<li><span class="title">Phone</span><span class="value">{$user->renderPhoneLink()}</span></li>
									<li><span class="title">E-mail</span><span class="value">{$user->renderEmailLink()}</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="pt-page" id="portfolio">
				<div class="section-inner custom-page-content">
					<div class="page-header color-1">
						<h2>Portfolio</h2>
					</div>
					<div class="page-content">
						<div class="portfolio-content">
							<ul id="portfolio_filters" class="portfolio-filters">
								<li class="active">
									<a class="filter btn btn-link active" data-group="all">All</a>
								</li>
								{foreach $user->projects as $m}
									<li>
										<a class="filter btn btn-link" data-group="media">Media</a>
									</li>
								{/foreach}
							</ul>
							<div id="portfolio_grid" class="portfolio-grid portfolio-masonry masonry-grid-3">
								{foreach $user->projects as $m}
									<figure class="item" data-groups="['all', 'media']">
										<a class="" href="" data-toggle="modal" data-target="#project-modal-{$m->id}">
											<img src="{$m->getImageUrl()}" alt="{$m->name|escape} project image.">
											<div>
												<h5 class="name">{$m->name|escape}</h5>
												<small></small>
												<i class="fa fa-file-text-o"></i>
											</div>
										</a>
									</figure>
								{/foreach}
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="pt-page" id="experience">
				<div class="section-inner custom-page-content">
					<div class="page-header color-1">
						<h2>Experience</h2>
					</div>
					<div class="page-content">
						<div class="block">
							{foreach $user->jobs as $m}
								<div class="timeline">
									<div class="timeline-item">
										<h4 class="item-title">{$m->position|escape}</h4>
										<span class="item-period">{$m->getDatesString()}</span>
										<span class="item-small">{$m->company_name|escape}</span>
										<p class="item-description">{$m->company_description|escape}</p>
										<div>{$m->description}</div>
									</div>
								</div>
							{/foreach}
						</div>
					</div>
				</div>
			</section>

			<section class="pt-page" id="education">
				<div class="section-inner custom-page-content">
					<div class="page-header color-1">
						<h2>Education</h2>
					</div>
					<div class="page-content">
						<div class="block">
							<div class="timeline">
								{foreach $user->education as $m}
									<div class="timeline-item">
										<h4 class="item-title"></h4>
										<span class="item-period">{$m->getDatesString()}</span>
										<span class="item-small">{$m->institute_name|escape}</span>
										<p class="item-description">{$m->description|escape}</p>
									</div>
								{/foreach}
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="pt-page" id="skills">
				<div class="section-inner custom-page-content">
					<div class="page-header color-1">
						<h2>Skills</h2>
					</div>
					<div class="page-content">
						<div class="row">
							{foreach $user->getSkills() as $m}
								<div class="col-sm-6 col-md-6 col-lg-6">
									<div class="block">
										<div class="block-title">
											<h3>{$m->name|escape}</h3>
											<div class="skills-info">
												<p>{$m->level|escape}</p>
												<p>{$m->description|escape}</p>
											</div>
										</div>
									</div>
								</div>
							{/foreach}
						</div>
						{if $user->hasResume()}
							<div class="row">
								<div class="col-sm-12 col-md-12 col-lg-12">
									<div class="block">
										<div class="center download-resume">
											<a href="{$user->getResumeUrl()}" class="btn btn-secondary">Download Resume</a>
										</div>
									</div>
								</div>
							</div>
						{/if}
					</div>
				</div>
			</section>

			<section class="pt-page" id="contact">
				<div class="section-inner custom-page-content">
					<div class="page-header color-1">
						<h2>Contact</h2>
					</div>
					<div class="page-content">

						<div class="row">
							<div class="col-sm-6 col-md-6">
								<div class="block-title">
									<h3>Get in <span>Touch</span></h3>
								</div>
								<div class="contact-info-block">
									<div class="ci-icon">
										<i class="fa fa-map-marker"></i>
									</div>
									<div class="ci-text">
										<h5>{$user->location|escape}</h5>
									</div>
								</div>
								<div class="contact-info-block">
									<div class="ci-icon">
										<i class="fa fa-envelope"></i>
									</div>
									<div class="ci-text">
										<h5>{$user->renderEmailLink()}</h5>
									</div>
								</div>
								<div class="contact-info-block">
									<div class="ci-icon">
										<i class="fa fa-phone"></i>
									</div>
									<div class="ci-text">
										<h5>{$user->renderPhoneLink()}</h5>
									</div>
								</div>
							</div>

							<div class="col-sm-6 col-md-6">
								<div class="block-title">
									<h3>Contact <span>Form</span></h3>
								</div>
								<form id="contact-form" method="post" action="contact_form/contact_form.php">

									<div class="messages"></div>

									<div class="controls">
										<div class="form-group form-group-with-icon">
											<i class="fa fa-user"></i>
											<label>Full Name</label>
											<input id="form_name" type="text" name="name" class="form-control" placeholder required="required" data-error="Name is required.">
											<div class="form-control-border"></div>
											<div class="help-block with-errors"></div>
										</div>

										<div class="form-group form-group-with-icon">
											<i class="fa fa-envelope"></i>
											<label>Email Address</label>
											<input id="form_email" type="email" name="email" class="form-control" placeholder required="required" data-error="Valid email is required.">
											<div class="form-control-border"></div>
											<div class="help-block with-errors"></div>
										</div>

										<div class="form-group form-group-with-icon">
											<i class="fa fa-comment"></i>
											<label>Message for Me</label>
											<textarea id="form_message" name="message" class="form-control" placeholder rows="4" required="required" data-error="Please, leave me a message."></textarea>
											<div class="form-control-border"></div>
											<div class="help-block with-errors"></div>
										</div>


										<input type="submit" class="button btn-send" value="Send message">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>

		</div>
	</div>
</div>

{foreach $user->projects as $m}
	{$m->renderModal()}
{/foreach}
