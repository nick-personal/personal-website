<?php

namespace app\commands;

use yii\console\Controller;

use app\models\{
	User,
	Skill,
	Project,
	Job,
	Education,
};

class CreateController extends Controller
{
	protected $data = [];


	public function actionIndex()
	{
		$filename = realpath(__DIR__ . '/../data/data.json');
		$jsonData = file_get_contents($filename);
		$this->data = json_decode($jsonData, true);

		$this->createUser();
		$this->createEntries();

		return 0;
	}

	protected function createUser(): void
	{
		$user = new User();
		$data = $this->data['user'];
		$user->setAttributes($data);
		$user->save();
	}

	protected function createEntries(): void
	{
		$entries = [
			'projects' => Project::class,
			'jobs' => Job::class,
			'education' => Education::class,
			'skills' => Skill::class,
		];

		foreach ($entries as $entry => $class)
		{
			$data = $this->data[$entry];
			foreach ($data as $i)
			{
				$model = new $class();
				$model->setAttributes($i);
				$model->save();
			}
		}
	}


}
